import React, { Dispatch, SetStateAction, useContext } from "react";

import { TrashButton } from "components/Button/TrashButton";

import { OperationsStoreContext } from "stores/OperationsStore";
import { transactionsAPI } from "services/ApiService";

type Props = {
  id?: string;
  severalIds?: string[];
  setSidebarIsShown?: Dispatch<SetStateAction<boolean>>;
  setIsDataChanged?: Dispatch<SetStateAction<boolean>>;
  setSidebarWithCheckedTransactionsIsShown?: Dispatch<SetStateAction<boolean>>;
  checkedTransactions: string[];
  setCheckedTransactions: Dispatch<SetStateAction<string[]>>;
};

const DeleteTransaction: React.FC<Props> = ({
  id,
  setSidebarIsShown,
  severalIds,
  setIsDataChanged,
  setSidebarWithCheckedTransactionsIsShown,
  checkedTransactions,
  setCheckedTransactions,
}) => {
  const OperationsStore = useContext(OperationsStoreContext);

  const unCheckTransaction = (id: string) => {
    const idx = checkedTransactions.findIndex((el: string) => el === id);
    setCheckedTransactions([
      ...checkedTransactions.slice(0, idx),
      ...checkedTransactions.slice(idx + 1),
    ]);
  };

  const onDeleteTransaction = () => {
    if (id) {
      transactionsAPI.deleteTransaction(id).then(response => {
        if (!response.__v) {
          unCheckTransaction(id);
          OperationsStore.deleteTransaction(id);
          setSidebarIsShown && setSidebarIsShown(false);
        }
      });
    }
  };

  const onDeleteSeveralTransactions = () => {
    if (severalIds) {
      Promise.all(
        severalIds.map(transactionId =>
          transactionsAPI.deleteTransaction(transactionId),
        ),
      ).then(response => {
        if (!response[0].__v) {
          severalIds.map(id => {
            unCheckTransaction(id);
            OperationsStore.deleteTransaction(id);
          });
          setIsDataChanged && setIsDataChanged(prevState => !prevState);
          setSidebarWithCheckedTransactionsIsShown &&
            setSidebarWithCheckedTransactionsIsShown(false);
        }
      });
    }
  };

  if (id) {
    return (
      <div onClick={onDeleteTransaction}>
        <TrashButton />
      </div>
    );
  } else {
    return (
      <div onClick={onDeleteSeveralTransactions}>
        <TrashButton />
      </div>
    );
  }
};

export { DeleteTransaction };
