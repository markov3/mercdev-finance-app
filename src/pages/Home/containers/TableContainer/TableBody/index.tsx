import React, { useContext } from "react";
import { observer } from "mobx-react";

import { TableVirtualBody } from "pages/Home/components/Table/TableVirtualBody";
import { TablePlaceholder } from "pages/Home/containers/TableContainer/TablePlaceholder";
import {
  TableData,
  TableDataProps,
} from "pages/Home/containers/TableContainer/TableData";

import { OperationsStoreContext } from "stores/OperationsStore";

const TableBody: React.FC<TableDataProps> = observer(({ ...props }) => {
  const OperationsStore = useContext(OperationsStoreContext);

  return (
    <TableVirtualBody>
      {OperationsStore.isLoading ? (
        <TablePlaceholder />
      ) : (
        <TableData {...props} />
      )}
    </TableVirtualBody>
  );
});

export { TableBody };
