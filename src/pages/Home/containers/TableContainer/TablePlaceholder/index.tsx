import React from "react";

import { TableVirtualBody } from "pages/Home/components/Table/TableVirtualBody";
import { TablePlaceholderContainer } from "./styled";
import { Spinner } from "evergreen-ui";

const TablePlaceholder = () => {
  return (
    <TableVirtualBody>
      <TablePlaceholderContainer>
        <Spinner width={150} />
      </TablePlaceholderContainer>
    </TableVirtualBody>
  );
};

export { TablePlaceholder };
