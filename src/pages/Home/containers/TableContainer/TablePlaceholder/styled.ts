import styled from "styled-components";

const TablePlaceholderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 150px;
`;

export { TablePlaceholderContainer };
