import React, { Dispatch, SetStateAction } from "react";

import { TransactionElementStyle } from "./styled";

type Props = {
  id: string;
  checkedTransactions: string[];
  setCheckedTransactions: Dispatch<SetStateAction<string[]>>;
};

const TransactionElement: React.FC<Props> = ({
  children,
  checkedTransactions,
  setCheckedTransactions,
  id,
}) => {
  const onClickHandler = () => {
    const idx = checkedTransactions.findIndex((el: string) => el === id);

    setCheckedTransactions([
      ...checkedTransactions.slice(0, idx),
      ...checkedTransactions.slice(idx + 1),
    ]);
  };

  return (
    <TransactionElementStyle onClick={onClickHandler}>
      {children}
    </TransactionElementStyle>
  );
};

export { TransactionElement };
