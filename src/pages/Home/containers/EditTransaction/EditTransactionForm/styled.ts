import { designSystem } from "constants/designSystem";
import { createGlobalStyle } from "styled-components";

const GlobalStyleTransaction = createGlobalStyle`
  .css-1dccw9w[data-state="entered"] {
    background: ${designSystem.bgColor.white};
  }
  .ub-color_101840 {
    color: ${designSystem.color.nightBlue};
  }
  .ub-bg-clr_white {
    background-color: ${designSystem.bgColor.white};
  }
  .ub-color_474d66 {
    color: ${designSystem.color.nightBlue};
  }
  .css-12b25td:not([disabled])[data-active]{
    background: #d3d7e889;
  }
  .css-9r9n92[aria-selected="true"]{
    background: #d3d7e889;
  }
`;

export { GlobalStyleTransaction };
