import React from "react";

import { OperationFiltersSection } from "../OperationFiltersSection";
import TableContainer from "../TableContainer";

const TableWithFilters = () => {
  return (
    <>
      <OperationFiltersSection />
      <TableContainer />
    </>
  );
};

export { TableWithFilters };
