import React from "react";

import { Dialog as DialogUI, DialogProps } from "evergreen-ui";

import { IDataModel } from "stores/OperationsStore";

export type DialogPropsType = { transactionInfo?: IDataModel } & DialogProps;

const Dialog: React.FC<DialogPropsType> = ({ children, ...restProps }) => {
  return <DialogUI {...restProps}>{children}</DialogUI>;
};

export { Dialog };
