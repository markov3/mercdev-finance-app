import styled from "styled-components";

import { Table } from "evergreen-ui";

import { designSystem } from "constants/designSystem";

const StyledTableVirtualBody = styled(Table.VirtualBody)`
  height: 718px;
  & div > div:first-child {
    border-top-left-radius: ${designSystem.borderRadius["24"]};
    border-top-right-radius: ${designSystem.borderRadius["24"]};
  }
`;

export { StyledTableVirtualBody };
