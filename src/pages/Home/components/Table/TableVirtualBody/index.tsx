import React from "react";

import { StyledTableVirtualBody } from "./styled";

type Props = {};

const TableVirtualBody: React.FC<Props> = ({ children }) => {
  return <StyledTableVirtualBody>{children}</StyledTableVirtualBody>;
};

export { TableVirtualBody };
