import React, { useEffect, useState } from "react";
import { SubmitHandler } from "react-hook-form";

import { Pane } from "components/Pane";
import { Button } from "components/Button";
import { AuthForm } from "./containers/AuthForm";
import { StyledAuthForm, StyledError } from "./styled";

import { authAPI } from "services/ApiService";

import { InputsProps } from "./types/inputs";

const Auth = () => {
  const [isAuth, setIsAuth] = useState(true);
  const [toggleForm, setToggleForm] = useState(true);
  const [error, setError] = useState("");
  const [userName, setUserName] = useState<string>("");

  const onSubmitRegister: SubmitHandler<InputsProps> = credentialsData => {
    authAPI
      .register(credentialsData.username, credentialsData.password)
      .then(response => {
        setUserName(response.username);
        setToggleForm(true);
      })
      .catch(error => {
        setError(error.response.data);
        setUserName("");
      });
  };

  const onSubmitLogin: SubmitHandler<InputsProps> = credentialsData => {
    authAPI
      .login(credentialsData.username, credentialsData.password)
      .then(data => {
        localStorage.setItem("token", data.token);
        if (localStorage.getItem("token")) {
          localStorage.setItem("username", credentialsData.username);
          location.assign("/");
        }
      })
      .catch(error => setError(error.response.data.message));
  };

  useEffect(() => {
    if (localStorage.getItem("username")) {
      setIsAuth(true);
    } else {
      setIsAuth(false);
    }
  }, [localStorage.getItem("username")]);

  return (
    <StyledAuthForm>
      <Pane display="flex" gap="64px">
        <Pane display="flex" gap="16px" flexDirection="column">
          {isAuth ? (
            <>
              <p>Hello {localStorage.getItem("username")}!</p>
              <Button
                onClick={() => {
                  localStorage.removeItem("username");
                  localStorage.removeItem("token");
                  location.reload();
                  setToggleForm(true);
                }}
              >
                Log out
              </Button>
            </>
          ) : (
            <>
              {toggleForm ? (
                <>
                  <AuthForm
                    userName={userName}
                    title="Sign in"
                    onSubmit={onSubmitLogin}
                  />
                  <Button onClick={() => setToggleForm(false)}>Register</Button>
                </>
              ) : (
                <>
                  <AuthForm
                    userName={userName}
                    title="Sign up"
                    onSubmit={onSubmitRegister}
                  />
                  <Button onClick={() => setToggleForm(true)}>Login</Button>
                </>
              )}
            </>
          )}
        </Pane>
      </Pane>
      {error && !userName ? (
        <StyledError>{error.toUpperCase()}</StyledError>
      ) : (
        <></>
      )}
    </StyledAuthForm>
  );
};

export { Auth };
