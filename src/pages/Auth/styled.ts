import styled from "styled-components";

import { Pane } from "evergreen-ui";

import { designSystem } from "constants/designSystem";

const StyledAuthForm = styled(Pane)`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: ${designSystem.indent[32]};
`;

const StyledError = styled.p`
  &:before {
    content: "⚠ ";
  }
  color: ${designSystem.text.color.warning};
  font-family: "Segoe UI";
`;

export { StyledAuthForm, StyledError };
