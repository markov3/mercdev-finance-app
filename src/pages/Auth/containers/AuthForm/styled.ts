import styled from "styled-components";

import { designSystem } from "constants/designSystem";

const StyledError = styled.p`
  &:before {
    content: "⚠ ";
  }
  color: ${designSystem.text.color.warning};
  font-size: ${designSystem.text.fontSize[14]};
  font-family: "Segoe UI", sans-serif;
`;

const UserContainer = styled.div`
  font-size: ${designSystem.text.fontSize["18"]};
  color: ${designSystem.text.color.success};
  text-align: center;
`;

export { StyledError, UserContainer };
