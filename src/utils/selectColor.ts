import { designSystem } from "constants/designSystem";

const selectColor = (children: string) => {
  switch (children) {
    case "Clothes":
      return "#FFD3C9";
    case "Food":
      return "#FFEAC2";
    case "Home service":
      return "#F0E3FF";
    case "Health":
      return "#CDF9E1";
    case "Restaurants":
      return "#FFE5F3";
    case "Entertainment":
      return "#F0E3FF";
    case "Salary":
      return "#DDEFFF";
    case "Transport":
      return "#FFE5F3";
    case "Credits":
      return "#dde8f0";
    case "Gifts":
      return "#85c8de";
    case "Benefit":
      return "#b1daea";
    case "Aid":
      return "#dae5ea";
    case "Award":
      return "#64ac8f";
    case "Dues":
      return "#94d6ba";
    case "Petrol":
      return "#e7f5dc";
    case "Phone":
      return "#c0dfc2";
    case "Public service":
      return "#D9FFFF";
    case "Kindergarten":
      return "#0195ae";
    case "Gym":
      return "#85c8de";
    case "Insurance":
      return "#b1daea";
    case "Vacation":
      return "#dae5ea";
    case "Repairs":
      return "#64ac8f";
    case "Medicine":
      return "#94d6ba";
    case "Travel":
      return "#B3FFFC";
    case "Home appliances":
      return "#97cbdc";
    case "Books":
      return "#018abd";
    case "Furniture":
      return "#018abd";
    default:
      return `${designSystem.color.light}`;
  }
};

export { selectColor };
