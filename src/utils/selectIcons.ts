import Food from "assets/icons/food.svg";
import Home from "assets/icons/home.svg";
import Default from "assets/icons/defaultIcon.svg";

const selectIcons = (children: string) => {
  switch (children) {
    case "Food":
      return Food;
    case "Home service":
      return Home;
    default:
      return Default;
  }
};

export { selectIcons };
