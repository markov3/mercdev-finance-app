import { designSystem } from "constants/designSystem";
import styled from "styled-components";

const StyledText = styled.p`
  text-transform: uppercase;
  color: ${designSystem.color.normal};
`;

export { StyledText };
