import React from "react";

import { Pane } from "components/Pane";
import { StyledText } from "./styled";

import { designSystem } from "constants/designSystem";

const Footer = () => {
  return (
    <footer>
      <Pane
        padding={designSystem.size[16]}
        display="flex"
        justifyContent="center"
      >
        <StyledText>С любовью от Mercury Development</StyledText>
      </Pane>
    </footer>
  );
};

export { Footer };
