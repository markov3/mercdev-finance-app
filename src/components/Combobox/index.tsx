import React from "react";

import { Combobox as ComboboxUI, ComboboxProps } from "evergreen-ui";

const Combobox: React.FC<ComboboxProps> = props => {
  return <ComboboxUI {...props} />;
};

export { Combobox };
