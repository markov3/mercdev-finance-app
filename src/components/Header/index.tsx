import React, { useEffect, useState } from "react";

import { Pane } from "../Pane";
import { Profile } from "../Profile";
import { SettingsButton } from "../Button/SettingsButton";
import ToggleSwitch from "components/Button/ToggleSwitch";
import { HeaderLinks, StyledNavLink } from "./styled";

import { navigationRoutes } from "constants/navigationRoutes";
import { designSystem } from "constants/designSystem";

import Logo from "assets/icons/logo.svg";

const Header = () => {
  const [profileName, setProfileName] = useState<any>("Profile");
  const isAuth = localStorage.getItem("token");

  useEffect(() => {
    if (localStorage.getItem("username")) {
      setProfileName(localStorage.getItem("username"));
    } else {
      setProfileName("Profile");
    }
  }, [localStorage.getItem("username")]);

  return (
    <Pane
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      paddingY={designSystem.indent[32]}
    >
      <Pane display="flex" alignItems="center" gap={designSystem.indent[86]}>
        <StyledNavLink exact to={localStorage.getItem("token") ? "/" : "/auth"}>
          <img src={Logo} alt="Finance App" />
        </StyledNavLink>

        <nav>
          <HeaderLinks>
            {navigationRoutes.map(({ path, title }) => (
              <li key={path}>
                {isAuth ? (
                  <StyledNavLink exact to={path} activeClassName={"active"}>
                    {title}
                  </StyledNavLink>
                ) : (
                  <StyledNavLink
                    exact
                    to={isAuth ? path : "/auth"}
                    activeClassName={path === "/auth" ? "active" : ""}
                  >
                    {title}
                  </StyledNavLink>
                )}
              </li>
            ))}
          </HeaderLinks>
        </nav>
      </Pane>

      <Pane display="flex" alignItems="center" gap={designSystem.indent[32]}>
        <StyledNavLink exact to={"/auth"}>
          <Profile name={profileName} />
        </StyledNavLink>
        <SettingsButton />
        <ToggleSwitch />
      </Pane>
    </Pane>
  );
};

export { Header };
