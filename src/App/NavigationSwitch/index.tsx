import React from "react";
import { Switch, Route } from "react-router-dom";

import { Home } from "pages/Home";
import { navigationRoutes } from "constants/navigationRoutes";

const NavigationSwitch = () => {
  return (
    <Switch>
      {navigationRoutes.map(({ path, component }) =>
        component ? (
          <Route key={path} exact path={path} component={component} />
        ) : (
          <Route key={path} exact path={path} />
        ),
      )}
      <Route component={Home} />
    </Switch>
  );
};

export { NavigationSwitch };
