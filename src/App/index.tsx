import React, { useContext, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import { MainContainer } from "./styled";
import { Header } from "components/Header";
import { NavigationSwitch } from "./NavigationSwitch";
import { Footer } from "components/Footer";
import { Pane } from "components/Pane";
import { OperationsStoreContext } from "../stores/OperationsStore";

const App = () => {
  const OperationsStore = useContext(OperationsStoreContext);

  useEffect(() => {
    OperationsStore.fetch();
  }, []);

  return (
    <MainContainer>
      <Pane
        maxWidth="1370px"
        minHeight="100vh"
        marginX="auto"
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
      >
        <Router>
          <Pane>
            <Header />
            <NavigationSwitch />
          </Pane>

          <Footer />
        </Router>
      </Pane>
    </MainContainer>
  );
};

export { App };
