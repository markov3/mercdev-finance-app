import styled from "styled-components";

import { designSystem } from "constants/designSystem";

const MainContainer = styled.div`
  font-weight: ${designSystem.text.weight.normal};
  font-size: ${designSystem.text.fontSize[16]};
  color: ${designSystem.text.color.normal};
`;

export { MainContainer };
