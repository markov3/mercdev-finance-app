import axios from "axios";

import {
  TransactionTypes,
  EditTransactionType,
} from "./types/transactionTypes";

const instance = axios.create({
  headers: {
    Authorization: `Bearer ${localStorage.token}`,
  },
  baseURL: "https://mercdev-finance-server.herokuapp.com/api/",
});

export const authAPI = {
  register(username: string, password: string) {
    return instance
      .post("registration", { username, password })
      .then(response => response.data);
  },

  login(username: string, password: string) {
    return instance
      .post("login", { username, password })
      .then(response => response.data);
  },
};

export const transactionsAPI = {
  getAllTransactions() {
    return instance.get("transactions").then(response => response.data);
  },

  createTransaction(data: TransactionTypes) {
    return instance
      .post("transactions", {
        date: data.date,
        category: data.category,
        description: data.description,
        quantity: data.quantity,
        type: data.type,
      })
      .then(response => response.data);
  },

  editTransaction(data: EditTransactionType) {
    return instance
      .put("transactions", {
        _userId: data._userId,
        _id: data._id,
        date: data.date,
        category: data.category,
        description: data.description,
        quantity: data.quantity,
        type: data.type,
      })
      .then(response => response.data);
  },

  deleteTransaction(id: string) {
    return instance
      .patch(`transactions?id=${id}`)
      .then(response => response.data);
  },
};
