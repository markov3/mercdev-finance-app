import { Home } from "pages/Home";
import { Info } from "pages/Info";
import { Auth } from "pages/Auth";

const navigationRoutes = [
  { path: "/", title: "Home", component: Home },
  { path: "/info", title: "Info", component: Info },
  { path: "/auth", title: "Authentication", component: Auth },
];

export { navigationRoutes };
